package mercurievv.skreep.pluginapi

import org.scalatest.FunSuite

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.8.11
 * Time: 18:53
 */
class Generators$Test extends FunSuite {
  test("Test sample shifting") {
    val ArraySize: Double = 10
    expect(ArraySize) {
      Generators.getSampleShift(1, 100, ArraySize) * 100
    }
    expect(0.1) {
      Generators.getSampleShift(1, 100, 10)
    }
    expect(0.2) {
      Generators.getSampleShift(2, 100, 10)
    }
    expect(2.0) {
      Generators.getSampleShift(2, 100, 100)
    }
  }

  test("Calculate value") {
    expect(-1) {
      Generators.calculateValue(0, 10, 100, Generators.SAWTOOTH)
    }
    expect(0.7998046875) {
      Generators.calculateValue(9, 10, 100, Generators.SAWTOOTH)
    }
  }

  test("Sine"){
    val precalculated: Array[Double] = Generators.getSinePrecalculated(8)
    println(precalculated.mkString(", "))
  }
}
