package mercurievv.skreep.pluginapi

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.8.11
 * Time: 16:37
 */
object Generators {
  private val WAVE_SIZE = 4096
  // samples (must be power of 2 here)
  val SAWTOOTH: Array[Float] = new Array[Float](WAVE_SIZE)
  val PULSE: Array[Float] = new Array[Float](WAVE_SIZE)
  val SINE: Array[Double] = getSinePrecalculated(WAVE_SIZE)


  def getSinePrecalculated(arraySize: Int): Array[Double] = {
    val halfOfArraySize: Int = arraySize / 2
    Array.range(-halfOfArraySize, halfOfArraySize)
      .map(_.toDouble / halfOfArraySize)
      .map(t => Math.sin(Math.PI * t))
  }

  val wh = WAVE_SIZE / 4; // 1:3 pulse
  for (i <- 0 until WAVE_SIZE) {
    SAWTOOTH(i) = -1f + (2f * (i.toFloat / WAVE_SIZE))
    PULSE(i) = if (i < wh) -1f else 1f
  }

  def calculateSawtoothValue(time: Long, frequency: Double, sampleRate: Int): Double = calculateValue(time, frequency, sampleRate, SAWTOOTH)

  def calculatePulseValue(time: Long, frequency: Double, sampleRate: Int): Double = calculateValue(time, frequency, sampleRate, PULSE)

  def calculateValue(time: Long, frequency: Double, sampleRate: Int, values: Array[Float]): Double = {
    val shift: Double = getSampleShift(frequency, sampleRate, WAVE_SIZE)
    val absoluteSampleTime: Long = (time * shift).toLong
    values((absoluteSampleTime % values.size).toInt)
  }
  def calculateValue(time: Long, frequency: Double, sampleRate: Int, values: Array[Double]): Double = {
    val shift: Double = getSampleShift(frequency, sampleRate, WAVE_SIZE)
    val absoluteSampleTime: Long = (time * shift).toLong
    values((absoluteSampleTime % values.size).toInt)
  }

  def getSampleShift(frequency: Double, sampleRate: Double, arraySize: Double): Double = {
    arraySize / (sampleRate / frequency)
  }
}
