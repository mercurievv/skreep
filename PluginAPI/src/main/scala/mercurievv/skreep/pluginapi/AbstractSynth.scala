package mercurievv.skreep.pluginapi

import mercurievv.skreep.host.interfaces.Synth
import jvst.wrapper.valueobjects.{VSTMidiEvent, VSTEvent, VSTEvents}
import mercurievv.skreep.pluginapi.{Generators, NoteFrequencyCalculator, NoteEvent, NoteEventsProcessor}

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.1.11
 * Time: 02:40
 */
abstract class AbstractSynth extends Synth {
  private val NUM_FREQUENCIES = 128
  // 128 midi notes
  var noteEventsProcessor = new NoteEventsProcessor
  var globalTime: Long = 0
  var SAMPLE_RATE = 44000
  //var envelope = new Envelope
  var currentNote = NoteEvent.NoteOff

  def generateHarmonicas(harmonicas: List[(Double, Double)]): Iterator[(Double, Double)]={
    def generate (list: List[(Double, Double)]) : List[(Double, Double)] = {
      List(list(0), list(1))
    }
    harmonicas.sliding(2).flatMap(generate)
  }

  def processReplacing(inputs: Array[Array[Float]], outputs: Array[Array[Float]], sampleFrames: Int) {
    for (x <- 0 until outputs(0).length) {
      globalTime += 1
      val aNote: Option[NoteEvent] = noteEventsProcessor.getNote(x)
      aNote.map(note => currentNote = note)
      val frequency: Double = NoteFrequencyCalculator.getNoteFrequency(currentNote.note)
      val sampleValue: Double = calculateSampleValue(frequency, globalTime, SAMPLE_RATE)



      outputs(0)(x) = (sampleValue * currentNote.velocity).toFloat
      outputs(1)(x) = (sampleValue * currentNote.velocity).toFloat
    }
  }

  def calculateSampleValue(frequency: Double, time: Long, sample_rate: Int): Double

  def processEvents(events: VSTEvents): Int = {
    val midiEvents: Array[VSTMidiEvent] = events.getEvents
      .filter(ev => ev.getType == VSTEvent.VST_EVENT_MIDI_TYPE)
      .map(event => event.asInstanceOf[VSTMidiEvent])
    noteEventsProcessor.processEvents(midiEvents)
    1
  }
}
