package mercurievv.skreep.pluginapi

import scala.Array
import jvst.wrapper.valueobjects.VSTMidiEvent

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.9.11
 * Time: 09:55
 */
class NoteEventsProcessor {

  var noteEventsStack: List[VSTMidiEvent] = List[VSTMidiEvent]()

  def getNote(currentFrame: Int): Option[NoteEvent] = {
    val possibleCurrentEvent: Option[VSTMidiEvent] = noteEventsStack.find(ev => ev.getDeltaFrames == currentFrame)
    noteEventsStack = noteEventsStack.filterNot(ev => ev.getDeltaFrames == currentFrame)
    possibleCurrentEvent.map(ev => NoteEvent.create(ev))
  }

  def processEvents(events: Array[VSTMidiEvent]) {
    val noteEvents = events.filter(ev => {
      val midiData: Array[Byte] = ev.getData
      val status: Int = midiData(0) & 0xf0
      status == 0x90 || status == 0x80 || status == 0xb0
    })
    noteEventsStack = noteEventsStack ++ noteEvents
  }
}

class NoteEvent(val note: Int, val velocity: Double){
}

object NoteEvent{
  val NoteOff = new NoteEvent(0, 0)

  def create(event: VSTMidiEvent): NoteEvent = {
    val midiData: Array[Byte] = event.getData
    val status: Int = midiData(0) & 0xf0
    val note: Int = midiData(1) & 0x7f
    val velocity: Double = status match {
      case 0x80 =>  0D
      case _ => (midiData(2) & 0x7f).toDouble / 128D
    }
    new NoteEvent(note, velocity)
  }
}

