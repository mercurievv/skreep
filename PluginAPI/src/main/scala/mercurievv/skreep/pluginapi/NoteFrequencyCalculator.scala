package mercurievv.skreep.pluginapi

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.9.11
 * Time: 14:20
 */
object NoteFrequencyCalculator {
  private final val NUM_FREQUENCIES: Int = 128
  private var FREQ_TAB: Array[Double] = new Array[Double](NUM_FREQUENCIES)
  val k: Double = 1.059463094359
  var a: Double = 6.875
  a *= k
  a *= k
  a *= k
  var i: Int = 0
  for (i <- 0 until NUM_FREQUENCIES) {
    FREQ_TAB(i) = a
    a *= k
  }


  def getNoteFrequency(note: Int): Double = {
    FREQ_TAB(note)
  }
}
