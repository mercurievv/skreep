package mercurievv.skreep.host

import interfaces.Synth
import org.osgi.framework.launch.{Framework, FrameworkFactory}
import java.util.ServiceLoader
import org.osgi.framework._
import scala.collection.JavaConverters._
import java.io.{PrintWriter, StringWriter, File}
import jvst.wrapper.VSTPluginAdapter
import java.util
import java.util.concurrent.TimeUnit
import org.slf4j.{Logger, LoggerFactory}

//import com.weiglewilczek.scalamodules._

import java.nio.file._
import java.nio.file
import java.nio.file.StandardWatchEventKinds._

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.27.10
 * Time: 03:53
 */
object SynthsJarLoader {
  private val log: Logger = LoggerFactory.getLogger("SynthsJarLoader")
  private var framework: Framework = null
  private var bundle: Bundle = null
  private var context: BundleContext = null
  val clazz: Class[FrameworkFactory] = classOf[FrameworkFactory]
  val SynthRootPath: String = new File(clazz.getProtectionDomain.getCodeSource.getLocation.getPath).getParentFile.getParentFile.getParentFile.getParentFile.getParentFile.getAbsolutePath
  log.info("Synth root is: " + SynthRootPath)
  print("Synth root is: " + SynthRootPath)
  val PathString: String = SynthRootPath + "\\TestSynth\\target"
  val BundlePath: String = "file:" + PathString + "\\TestSynth-1.0.jar"
  var synth: Synth = null

  def main(args: Array[String]) {
    initWatchingForSynths()
  }

  def initWatchingForSynths() {
    val loader: ServiceLoader[FrameworkFactory] = ServiceLoader.load(clazz, clazz.getClassLoader)
    val frameworkFactory: FrameworkFactory = loader.iterator().next()
    framework = {
      val map: Map[String, String] = Map(Constants.FRAMEWORK_SYSTEMPACKAGES_EXTRA -> "jvst.wrapper.valueobjects,mercurievv.skreep.host.interfaces")
      frameworkFactory.newFramework(map.asJava)
    }
    framework.start()
    context = framework.getBundleContext
    context installBundle "file:" + SynthRootPath + "\\TestSynth\\target\\TestSynth-1.0-bin\\lib\\scala-library-2.9.2.jar"

    startWatchingForSynthsFolder(framework)
  }


  private def startWatchingForSynthsFolder(framework: Framework) {
    log.info("Watch")
    bundle = context.getBundle(BundlePath)
    unloadSynth(bundle)
    loadSynthFromJar(BundlePath)

    new Thread(new Runnable {
      def run() {
        //pluginapi.uninstall()
        while (true) {
          if (classesDirectoryWasChanged()) {
            synth = null //todo use Mock instead of nulls
            unloadSynth(bundle)
            loadSynthFromJar(BundlePath)
          }
        }
        unloadSynth(bundle)
        framework.stop()
      }
    }) start()
  }


  def loadSynthFromJar(bundlePath: String) {
    try {
      log.info("Loading synth")
      bundle = startNewBundle(bundlePath, context)
      synth = getSynthInstance(context)
      log.info("Synth loaded")
    }
    catch {
      case ex: BundleException => {
        val sw = new StringWriter()
        val pw = new PrintWriter(sw)
        ex.printStackTrace(pw)
        log.info(sw.toString)
      }
    }
  }

  def getSynthInstance(context: BundleContext): Synth = {
    val sr: ServiceReference[Synth] = context.getServiceReference(classOf[Synth])
    val service: Object = context.getService(sr)
    service.asInstanceOf[Synth]
  }

  private def classesDirectoryWasChanged(): Boolean = {
    watchForDirectoryChange(PathString)
  }

  private def watchForDirectoryChange(pathString: String): Boolean = {
    log.info("Watch for folder change: " + pathString)
    val path: file.Path = Paths.get(pathString)
    val watchService = FileSystems.getDefault.newWatchService()
    try {
      def logEvents(events: util.List[WatchEvent[_]]) {
        events.asScala.foreach(e => {
          val event: WatchEvent[Path] = e.asInstanceOf[WatchEvent[Path]]
          log.info(event.context().toString + " " + event.kind().name())
        })
      }

      path.register(watchService, ENTRY_CREATE, ENTRY_MODIFY)
      val take: WatchKey = watchService.take()
      val events = take.pollEvents()
      logEvents(events)
      var key: WatchKey = null
      while ( {
        key = watchService.poll(1, TimeUnit.SECONDS); key != null
      })
        logEvents(key.pollEvents())

      log.info("File changes processed")
      return true
    }
    catch {
      case e: Exception => Thread.currentThread().interrupt()
    }
    false
  }

  private def unloadSynth(bundle: Bundle) {
    if (bundle == null)
      return
    if (bundle.getState == Bundle.UNINSTALLED)
      return
    bundle.stop()
    log.info("Bundle state before unloading: " + bundle.getState)
    bundle.uninstall()

  }

  private def startNewBundle(bundlePath: String, context: BundleContext): Bundle = {
    log.info("Installing bundle " + bundlePath)
    val bundle = context.installBundle(bundlePath)
    log.info("Bundle installed")
    bundle.start()
    log.info("Bundle started")

    bundle
  }
}
