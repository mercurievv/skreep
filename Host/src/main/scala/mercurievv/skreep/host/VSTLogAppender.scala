/*
package mercurievv.skreep.host

import org.slf4j.Logger
import org.apache.log4j.{WriterAppender, AppenderSkeleton}
import org.apache.log4j.spi.LoggingEvent
import jvst.wrapper.VSTPluginAdapter
import java.io.{StringWriter, Writer}

/**
 * Created with IntelliJ IDEA.
 * User: Victor Mercurievv
 * Date: 13.22.8
 * Time: 14:27
 * Contacts: email: mercurievvss@gmail.com Skype: 'grobokopytoff' or 'mercurievv'
 */
class VSTLogAppender extends WriterAppender  {
  private val writer: StringWriter = new StringWriter(){
    override def flush() {
      val string: String = toString
      string.lines.foreach(st => VSTPluginAdapter.log(st))
    }
  }
  setWriter(writer)
}
*/
