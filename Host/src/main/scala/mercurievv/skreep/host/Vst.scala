package mercurievv.skreep.host

import interfaces.Synth
import jvst.wrapper.VSTPluginAdapter
import jvst.wrapper.valueobjects.{VSTEvents, VSTPinProperties}
import jvst.wrapper.communication.VSTV20ToHost

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.1.11
 * Time: 08:07
 */
class Vst(val wrapper: Long) extends VSTPluginAdapter(wrapper) {
  val synthsJarLoader = SynthsJarLoader
  synthsJarLoader.initWatchingForSynths()
  VSTPluginAdapter.log("Construktor jVSTxSynth() INVOKED!")

  private final val NUM_OUTPUTS: Int = 2
  this.setProgram(0)
  this.setNumInputs(0)
  this.setNumOutputs(2)
  //this.hasVu(false); //deprecated as of vst2.4
  //this.hasClip(false); //deprecated as of vst2.4
  this.canProcessReplacing(true)
  this.isSynth(true)
  this.setUniqueID('j' << 24 | 'X' << 16 | 's' << 8 | 'y')
  this.suspend()

  def setParameter(p1: Int, p2: Float) {}

  def getParameter(p1: Int) = 0.0F

  def processReplacing(inputs: Array[Array[Float]], outputs: Array[Array[Float]], sampleFrames: Int) {
//    println(sampleFrames)
//    System.out.flush()
    val synth = synthsJarLoader.synth
    if (synth != null) //todo use mock instead of null
      synth.processReplacing(inputs, outputs, sampleFrames)
  }

  override def processEvents(e: VSTEvents) : Int = {
    val synth = synthsJarLoader.synth
    if (synth != null)
      return synth.processEvents(e)
    0
  }

  def getProgram = 0

  def setProgram(p1: Int) {}

  def setProgramName(p1: String) {}

  def getProgramName = ""

  def getParameterName(p1: Int) = ""

  def getParameterDisplay(p1: Int) = ""

  def getParameterLabel(p1: Int) = ""

  def getNumPrograms = 0

  def getNumParams = 0

  def canDo(feature: String) = {
    var ret: Int = VSTV20ToHost.CANDO_NO
    if (VSTV20ToHost.CANDO_PLUG_RECEIVE_VST_EVENTS == feature) ret = VSTV20ToHost.CANDO_YES
    if (VSTV20ToHost.CANDO_PLUG_RECEIVE_VST_MIDI_EVENT == feature) ret = VSTV20ToHost.CANDO_YES
    //if (VSTV20ToHost.CANDO_PLUG_MIDI_PROGRAM_NAMES == feature) ret = VSTV20ToHost.CANDO_YES
    ret
  }

  def setBypass(p1: Boolean) = false

  def string2Parameter(p1: Int, p2: String) = false

  def getProgramNameIndexed(p1: Int, p2: Int) = ""

  def getProductString = ""

  def getVendorString = ""

  def getPlugCategory = 0

  override def getOutputProperties(index: Int): VSTPinProperties = {
    var ret: VSTPinProperties = null
    if (index < NUM_OUTPUTS) {
      ret = new VSTPinProperties
      ret.setLabel("jVSTx " + (index + 1) + "d")
      ret.setFlags(VSTPinProperties.VST_PIN_IS_ACTIVE)
      if (index < 2) {
        ret.setFlags(ret.getFlags | VSTPinProperties.VST_PIN_IS_STEREO)
      }
    }
    ret
  }
}
