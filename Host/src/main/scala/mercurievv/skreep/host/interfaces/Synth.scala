package mercurievv.skreep.host.interfaces

import jvst.wrapper.valueobjects.VSTEvents

/**
 * Created by IntelliJ IDEA.
 * User: MercurieVV. email:mercurievvss@gmail.com skype: grobokopytoff or mercurievv
 * Date: 12.1.11
 * Time: 05:26
 */
trait Synth {
  def processEvents(events: VSTEvents) : Int

  def processReplacing(inputs: Array[Array[Float]], outputs: Array[Array[Float]], sampleFrames: Int)
}
