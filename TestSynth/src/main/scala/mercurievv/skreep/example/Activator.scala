/*
 * Copyright 2009-2011 Weigle Wilczek GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mercurievv.skreep.example

import org.osgi.framework.{BundleActivator, BundleContext}
import mercurievv.skreep.host.interfaces.Synth
import mercurievv.skreep.pluginapi.AbstractSynth

//import com.weiglewilczek.scalamodules._

class Activator extends BundleActivator {
  override def start(context: BundleContext) {
    val synth: Synth = new TestSynth
    context.registerService(classOf[Synth], synth, null)
    //context.createService(synth, interface1 = interface[Synth])
  }

  override def stop(context: BundleContext) {
    context
  }
}