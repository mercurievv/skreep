package mercurievv.skreep.example

import mercurievv.skreep.pluginapi.{AbstractSynth, Generators}

/**
 * Created with IntelliJ IDEA.
 * User: Victor Mercurievv
 * Date: 13.20.10
 * Time: 03:45
 * Contacts: email: mercurievvss@gmail.com Skype: 'grobokopytoff' or 'mercurievv'
 */
class SineSynth extends AbstractSynth {
  override def calculateSampleValue(frequency: Double, time: Long, sample_rate: Int): Double = {
    Generators.calculateValue(time, frequency, sample_rate, Generators.SINE)
  }
}
