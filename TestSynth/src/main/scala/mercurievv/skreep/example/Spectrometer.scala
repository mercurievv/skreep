package mercurievv.skreep.example

import javafx.application.Application
import javafx.stage.Stage
import javafx.scene.{Group, Scene}
import javafx.scene.canvas.{Canvas, GraphicsContext}
import javafx.scene.paint.Color
;
/**
 * Created with IntelliJ IDEA.
 * User: Victor Mercurievv
 * Date: 13.1.12
 * Time: 23:04
 * Contacts: email: mercurievvss@gmail.com Skype: 'grobokopytoff' or 'mercurievv'
 */
class Spectrometer extends Application{
  val root:Group = new Group()
  val canvas = new Canvas(300, 250)
  val gc:GraphicsContext = canvas.getGraphicsContext2D

  def drawShapes(hs: Iterable[Harmonica]){
    gc.setFill(Color.GREEN)
    gc.setStroke(Color.BLUE)
    gc.setLineWidth(5)
    getAmplitudesByFrequencies(hs).foreach(abf=>{
      gc.strokeLine(abf._1, abf._1, abf._2, abf._2)
    })
  }


  def getAmplitudesByFrequencies(hs: Iterable[Harmonica]): Map[Int, Double] = {
    val byFreqs: Map[Int, Iterable[Harmonica]] = hs.groupBy(h => (h.relFreq * 100d).ceil.toInt)
    byFreqs.map(bf => bf._1 -> bf._2.map(_.relAmplitude).sum)
  }

  def start(primaryStage: Stage) {

    primaryStage.setTitle("Drawing Operations Test")
//    drawShapes(gc)
    root.getChildren.add(canvas)
    primaryStage.setScene(new Scene(root))
    primaryStage.show()
  }
}
