package mercurievv.skreep.example

import mercurievv.skreep.pluginapi.{AbstractSynth, Generators}

/**
 * Created with IntelliJ IDEA.
 * User: Victor Mercurievv
 * Date: 13.20.10
 * Time: 03:45
 * Contacts: email: mercurievvss@gmail.com Skype: 'grobokopytoff' or 'mercurievv'
 */
class TestSynth extends AbstractSynth {
  private val firstGen: List[Harmonica] = List(Harmonica(1, 0.6), Harmonica(2, 0.5), Harmonica(3, 0.7))
  val harmonicas: Iterable[Harmonica] = processNTimes(firstGen, 3)
  println("h count " + harmonicas.size)

  def processNTimes(firstGen: List[Harmonica], times: Int): List[Harmonica] = {
    var res: List[Harmonica] = firstGen
    for (genNr <- 1 to times) {
      res = newGeneration(res)
    }
    res
  }


  def newGeneration(harmonicas: List[Harmonica]): List[Harmonica] = {
    val sliding: Iterator[List[Harmonica]] = harmonicas.sliding(2)
    val allHarmonicas: List[Harmonica] = harmonicas ++ sliding.flatMap(createNewHarmonicas).toList
    allHarmonicas.sortBy(_.relFreq)
  }


  def createNewHarmonicas(h: List[Harmonica]): List[Harmonica] = {
    val avgFreq: Double = h.map(_.relFreq).sum / h.length
    val avgAmp: Double = h.map(_.relAmplitude).sum / h.length
    val maxFreq: Harmonica = h.maxBy(_.relFreq)
    val minFreq: Harmonica = h.minBy(_.relFreq)
    List(
      new Harmonica(avgFreq, avgAmp * 1.6),
      new Harmonica(minFreq.relFreq * (4 / 3), avgAmp * 1.9),
      new Harmonica(maxFreq.relFreq * 2, maxFreq.relFreq * 0.4),
      new Harmonica(minFreq.relFreq / 2, minFreq.relFreq * 0.5)
    )
  }

  override def calculateSampleValue(frequency: Double, time: Long, sample_rate: Int): Double = {
    var smpl: Double = 0
    for (harmonica <- harmonicas) {
      smpl = smpl + (Generators.calculateValue(time, frequency * harmonica.relFreq, sample_rate, Generators.SINE) * harmonica.relAmplitude * 0.01)
    }
    smpl
  }
}

case class Harmonica(relFreq: Double, relAmplitude: Double) {
  var phase: Double = 0
}
